package ilayda.shapes;

public class Square {

	protected int side;

	public Square(int side) {
		this.side = side;

	}

	public int area() {
		return side * side;

	}


}
